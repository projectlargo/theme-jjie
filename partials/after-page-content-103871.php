<?php if(is_hub('103871')) { ?>
	<hr />
	<div class="hub-footer">
		<h6 style="margin-bottom:5px;">A Dedicated Team</h6>
		<div class="dedicated"><img src="/files/2013/02/dedicated-team.png"/></div><p><small>The Juvenile Justice Reform Hub is sponsored by a generous donation from the <a href="http://www.macfound.org/" target="_blank">John D. and Catherine T. MacArthur Foundation's</a> <a href="http://modelsforchange.net/" target="_blank">Models for Change</a> initiative. Hosted and promoted by the Juvenile Justice Information Exchange, content is curated by the <a href="http://www.njjn.org/" target="_blank">National Juvenile Justice Network</a>. For further information, comments, or suggestions for improvement, email <strong>info [at] njjn [dot] org</strong>. You can read our <a href="/hub/editorial-policy">editorial policy here</a>.</small></p>
	</div>
	<?php /* Javascript Called for Tab'd shortcode */ ?>
	<script src="<?php echo get_stylesheet_directory_uri() . '/js/jquery.ba-bbq.min.js'; ?>" type="text/javascript"></script>
	<script src="<?php echo get_stylesheet_directory_uri() . '/js/hub.js'; ?>" type="text/javascript"></script>
	<?php
}
